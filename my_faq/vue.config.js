module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        'process.env': JSON.stringify(require('dotenv').config().parsed),
      }),
    ],
  },
};
