import axios from 'axios';

const API_URL = 'http://localhost:8000/';

export class AuthService {
  static async login(credentials) {
    try {
      const response = await axios.post(API_URL + 'login/', credentials, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
      if (response.status == 200) {
        localStorage.setItem('user', JSON.stringify(response.data));
        return response.data;
      } else {
        throw new Error('Invalid username or password.');
      }
    } catch (error) {
      throw new Error('Failed to log in. Please try again.');
    }
  }

  static logout() {
    localStorage.removeItem('user');
  }

  static getCurrentUser() {
    try {
      const userString = localStorage.getItem('user');
      if (userString) {
        return JSON.parse(userString);
      } else {
        return null;
      }
    } catch (error) {
      console.error('Error retrieving user from local storage:', error);
      return null;
    }
  }
}
