# DevNgoFront My_FAQ with Vutify 

Project Name: My_FAQ

Technologies:

Vue.js 3
Vuetify
Repository URL: https://gitlab.com/Finoana/devngofront.git

Installation:

git clone https://gitlab.com/Finoana/devngofront.git
cd my_faq
yarn install

yarn dev
This will start the development server at http://localhost:3000.


Markdown
# My_FAQ - Vue.js 3 powered FAQ application

This project is a Vue.js 3 FAQ application built using Vuetify.

## Technologies

* Vue.js 3
* Vuetify

## Installation

```bash
git clone [https://gitlab.com/Finoana/devngofront.git](https://gitlab.com/Finoana/devngofront.git)
cd my_faq
yarn install
yarn dev

Users: username / password
Jhon / azerty
Fy / azerty
john_doe / strong_password123